package config

import (
	"github.com/spf13/viper"
)

var configs = make(map[string]*viper.Viper)

func Viper(name string) *viper.Viper {
	if configs[name] == nil {
		configs[name] = viper.New()
		configs[name].SetConfigName(name)
		configs[name].SetConfigType("json")
		configs[name].AddConfigPath(".")
		err := configs[name].ReadInConfig()
		if err != nil {
			panic(err)
		}
	}
	return configs[name]
}
