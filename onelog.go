package config

import (
	"os"

	"github.com/francoispqt/onelog"
)

var logger *onelog.Logger

func Log() *onelog.Logger {
	if logger == nil {
		switch Viper("config").GetString("environment") {
		case "development":
			logger = onelog.New(
				os.Stdout,
				onelog.ALL,
			)
		case "production":
			logger = onelog.New(
				os.Stdout,
				onelog.ERROR|onelog.FATAL,
			)
		default:
			panic("./config.json environment not set to development or production")
		}
	}
	return logger
}
